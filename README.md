[TOC]

## Отказоустойчивые распределенные системы




## План курса

Статьи: [docs](docs)

### Лекция 1: введение

Слайды с лекции: [pdf](lecture_01/main.pdf)

  * Модель вычислений: процессоры и сеть, аналогия с разделяемой памятью
  * синхронные/асинхронные вычисления
  * разделяемая память / message passing
  * 4 модели распределённых систем. Доказательство их идентичности. Синхронайзер.
  * Определение casual order / happens before (Lamport) для message-passing систем
  * Модель отказов сетей
  * Модель отказов процессов
  * Моделирование отказов сети через отказ процессов
  * Вложенность классов отказов друг в друга
  * Определение консенсуса, три его свойства
  * k-set consensus
  
### Лекция 2: Свойства асинхронных вычислений

Слайды с лекции: [pdf](lecture_02/main.pdf)

  * Теорема Фишера, Линч и Патерсона
  * Следствия из теоремы, альтернативные формулировки консенсуса
  * CAP теорема
  * Критика CAP теоремы
  * ACID
  * Степени изоляции транзакций

### Лекция 3: CP системы
  * Zookeeper
  * ZAB
  
### Лекция 4: AP системы  
  * DynamoDB
  
## Материалы

## Книги:
   ссылка: [ЯДиск](https://yadi.sk/d/Kw8fx07X3Q4cha)
   
### Модель распределённых вычислений   
  * Distributed Computing, Ajay Kshemkalyani: Chapter 1: Introduction
    * Definition
    * Relation to parallel multiprocessor/multicomputer systems
    * Message-passing systems versus shared memory systems
    * Synchronous versus asynchronous executions
    
  * Distributed Computing, Ajay Kshemkalyani: Chapter 2: A model of distributed computations
    * program model
    * casual order
    * network model
    * Models of process communications

### FLP теорема
  * [A Brief Tour of FLP Impossibility](https://the-paper-trail.org/blog/a-brief-tour-of-flp-impossibility/ )
  * Impossibility of Distributed Consensus with One Faulty Process - FLP (Fischer, Lynch, Paterson)
  * [mini-series on consensus, Adrian Colyer](https://blog.acolyer.org/2015/03/01/cant-we-all-just-agree/)
  
### СAP теорема
  * Brewer's Conjeture and the Feasibility of CAP web services - Seth Gilbert, Nancy Lynch
  * PODC-keynote
  * Spanner, TrueTime and the CAP Theorem- Google, Eric Brewer
  * [FLP and CAP aren’t the same thing](http://the-paper-trail.org/blog/flp-and-cap-arent-the-same-thing/)
  * [CAP theorem myths, Grigory Demchenko](https://gridem.blogspot.ru/2017/03/cap-theorem-myths.html#more)
  
### CAP системы
  * M. Burrows, "The Chubby Lock Service for Loosely-Coupled Distributed Systems," Proc. Symp. Operating Systems Design and Implementation (OSDI 06), Usenix, 2006, pp. 335-350.
  * [ZooKeeper: wait-free coordination for internet scale systems](https://blog.acolyer.org/2015/01/27/zookeeper-wait-free-coordination-for-internet-scale-systems/)
  * Zab: High-performance broadcast for primary-backup systems
  * [ZAB обзор от Adrian Colyer](https://blog.acolyer.org/2015/03/09/zab-high-performance-broadcast-for-primary-backup-systems/)
  * [ZAB особенности реализации от Adrian Colyer](https://blog.acolyer.org/2015/03/10/zookeepers-atomic-broadcast-protocol-theory-and-practice/), [оригинальная статья](http://www.tcs.hut.fi/Studies/T-79.5001/reports/2012-deSouzaMedeiros.pdf)
  * G. DeCandia et al., "Dynamo: Amazon’s Highly Available Key-Value Store," Proc. 21st ACM SIGOPS Symp. Operating Systems Principles (SOSP 07), ACM, 2007, pp. 205-220.


### Репликация в распеределённых системах
  * Replicated Data Consistency Explained Through Baseball - Doug Terry, Microsoft Research Silicon Valley

### MIT
  * http://css.csail.mit.edu/6.824/2014/labs/
  * git://g.csail.mit.edu/6.824-golabs-2014
  * http://css.csail.mit.edu/6.824/2014/schedule.html
  * [6.852: Distributed Algorithms](http://courses.csail.mit.edu/6.852/08/lecture.html)

## Семинары

### Семинар 1: Zookeeper

[Материалы](seminar_zookeeper.md)

## Домашние задания
